const mysql = require('mysql');
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "examenclientes",
});
connection.connect((error) => {
  if (error) {
    console.log('El error de conexion es : ' + error);
    return;
  }
  console.log('Estás CONECTADO de manera EXITOSA a la DB!');
});

// DECLARAMOS EL EXPRESS EN VARIABLES
const express = require('express');
const warp = express();

// CAPTURAMOS EL FORMULARIO
warp.use(express.urlencoded({ extended: false }));
warp.use(express.json());

//  IMPLEMENTAMOS LOS SIGUIENTE PARA USAR EJS
warp.set('view engine', 'ejs');

// TRAEMOS RES Y REQ DE EXPRESS
const req = require('express/lib/request');
const res = require('express/lib/response');
const { redirect } = require('express/lib/response');



warp.post("/", async (req, res) => {

  //VALIDACIONES 

  const expresiones = {
    cedula: /^\d{10}$/, // numérico de 10 dígitos.
    nombre: /^[a-zA-ZÀ-ÿ\s]{1,30}$/, //Texto de al menos 40 digitos.
    direccion: /^([a-zA-Z0-9_-]){1,40}$/, //Alfanumerico de al menos 70 digitos.
    telefono: /^\d{10}$/, // numérico de 2 dígitos.
    correo: /\S+@\S+.\S+/, // expresion definada en foros, correos.
  };

  document.querySelector("#btnenviar").addEventListener("click", validar);

  function validar() {

    var cedulaejs = document.getElementById("ci_ingreso").value;
    var nameejs = document.getElementById("name_ingreso").value;
    var direccionejs = document.getElementById("direccion_ingreso").value;
    var telefonoejs = document.getElementById("telefono_ingreso").value;
    var emailejs = document.getElementById("email_ingreso").value;


    var cedulaejs1 = expresiones.cedula.test(cedulaejs);
    var nameejs1 = expresiones.nombre.test(nameejs);
    var direccionejs1 = expresiones.direccion.test(direccionejs);
    var telefonoejs1 = expresiones.telefono.test(telefonoejs);
    var emailejs1 = expresiones.correo.test(emailejs);
  


    if (cedulaejs1 == true) {
      if (nameejs1 == true) {
        if (direccionejs1 == true) {
          if (telefonoejs1 == true) {
            if (emailejs1 == true) {
                  alert("Formulario exitoso");
                } else {
                  alert("La cédula debe ser de 10 dígitos.");
                }
              } else {
                alert("Nombre incorrecto");
              }
            } else {
              alert("No se admiten caracteres especiales");
            }
          } else {
            alert("Formato de teléfono incorrecto");
          }
        } else {
          alert("Correo invalido");
        }
      } 

  //SQL

  sqlingreso = req.body;
  connection.query("INSERT INTO cliente SET ?", {
    ci_cliente: sqlingreso.ci_ingreso,
    nombre_cliente: sqlingreso.name_ingreso,
    direccion_cliente: sqlingreso.direccion_ingreso,
    telefono_cliente: sqlingreso.telefono_ingreso,
    email_cliente: sqlingreso.email_ingreso,
  });
  res.render("crear");
});


// SE ESTABLECEN LAS RUTAS
warp.get("/", (req, res) => {
  res.render("crear");
});

// SE UBICA EL PUERTO POR DONDE NOS ESCUCHARÁ.

warp.listen(4000, (req, res) => {
  console.log("SERVER RUNNING IN http://localhost:4000");
});